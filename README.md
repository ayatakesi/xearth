Xearth live Wallpaper for Android

Xearth displays an image of the earth, as seen from your favorite vantage point
in space (this can be your current position), correctly shaded for the current
position of the sun. By default, xearth updates the displayed image every five
minutes. The time between updates as well as many parameters can be changed
using the settings.

This is a completely rewritten port of xearth V.1.1 for UNIX. 
See also: http://hewgill.com/xearth/original/



Xearth zeigt ein Bild der Erde von Ihrem Lieblings-Aussichtspunkt im Weltraum
aus gesehen, richtig beschattet f�r die aktuelle Position der Sonne.
Standardm��ig aktualisiert xearth das angezeigte Bild alle f�nf Minuten. Die
Zeit zwischen den Updates sowie viele Parameter k�nnen �ber die Einstellungen
ge�ndert werden.

Dies ist eine v�llig neu geschriebene Portierung von xearth V.1.1 f�r UNIX.
Siehe auch: http://hewgill.com/xearth/original/

LEGAL STUFF:

Parts of the source code of the Android port of xearth are 
    Copyright (C) 2011-2015 by Markus Hoffmann.
The Sources of the Android version of xearth are completely rewritten 
code based on the original sourcecode 
    Copyright (C) 1989, 1990, 1993-1995, 1999 by Kirk Lauritz Johnson
with the overlay extension based on code 
    Copyright (C) 1997-2006 by Greg Hewgill

Xearth live Wallpaper for Android is licensed under the MIT open source
license. See the file LICENSE for details.


The authors make no representations about the suitability of this
software for any purpose. It is provided "as is" without express
or implied warranty.

THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS,
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT
OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
